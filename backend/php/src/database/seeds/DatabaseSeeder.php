<?php

use App\Session;
use App\SessionTime;
use Carbon\Carbon;
use Faker\Generator;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /** @var Generator */
    private $faker;

    public function __construct(Generator $faker)
    {
        $this->faker = $faker;
    }

    public function run()
    {
        for ($i = 0; $i < 100; ++$i) {
            /** @var Session $session */
            $session = Session::query()->create([
                'name' => $this->faker->unique()->words(2, true),
            ]);

            for ($ii = 0; $ii < rand(1, 3); ++$ii) {
                $this->addTime($session);
            }
        }
    }

    private function addTime(Session $session)
    {
        $randomTimeInPast = $this->faker->dateTimeBetween('now - 6 months', 'now - 1 day');

        $from = Carbon::parse($randomTimeInPast->format('Y-m-d H:i:s'));
        $durationInMinutes = rand(10, 10 * 60);
        $to = $from->copy()->addMinutes($durationInMinutes);

        return SessionTime::query()->create([
            'session_id' => $session->getKey(),
            'from' => $from,
            'to' => $to,
            'total_time' => $durationInMinutes * 60,
        ]);
    }
}
