<?php

namespace App\Queries;

use App\SessionTime;
use Illuminate\Support\Facades\DB;

class AggregatedSessionTimesQuery {
    public function get(string $groupedBy)
    {
        $times = SessionTime::query()
            ->select([
                DB::raw($this->timeSelect($groupedBy)),
                DB::raw('sum(`total_time`) as `totalTime`'),
            ])
            ->groupBy('period')
            ->orderBy('period', 'desc')
            ->get();

        return $times;
    }

    private function timeSelect(string $groupedBy): string
    {
        $column = 'from';

        if ($groupedBy === 'month') {
            return $this->concatTimeSelect($column, '%Y-%m');
        }

        if ($groupedBy === 'week') {
            return $this->concatTimeSelect($column, '%Y-%U');
        }

        return $this->concatTimeSelect($column, '%Y-%m-%d');
    }

    private function concatTimeSelect(string $column, string $format): string
    {
        return "date_format(`$column`,'$format') as `period`";
    }
}