<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Session extends Model
{
    protected $table = 'sessions';
    protected $primaryKey = 'session_id';
    protected $guarded = [];
    
    public function times(): HasMany
    {
        return $this->hasMany(SessionTime::class, 'session_id', 'session_id');
    }
}
