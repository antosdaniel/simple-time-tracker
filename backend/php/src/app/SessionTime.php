<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SessionTime extends Model
{
    protected $table = 'session_times';
    protected $primaryKey = 'time_id';
    protected $guarded = [];
    public $timestamps = false;
}
