<?php

namespace App\Http\Controllers;

use App\Http\Requests\IndexRequest;
use App\Http\Requests\StoreRequest;
use App\Queries\AggregatedSessionTimesQuery;
use App\SessionRepository;
use Illuminate\Http\Response;

class SessionsController extends Controller
{
    public function index(IndexRequest $request, AggregatedSessionTimesQuery $query)
    {
        return response()->json(
            $query->get($request->groupBy())
        );
    }

    public function store(StoreRequest $request, SessionRepository $sessions)
    {
        $sessions->add(
            $request->name(),
            $request->periods()
        );

        return response()->json([], Response::HTTP_CREATED);
    }
}
