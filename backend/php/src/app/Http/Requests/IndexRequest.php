<?php

namespace App\Http\Requests;

class IndexRequest extends Request
{
    public function rules()
    {
        return [
            'groupBy' => ['in:day,week,month'],
        ];
    }

    public function groupBy(): string
    {
        return $this->input('groupBy', 'day');
    }
}
