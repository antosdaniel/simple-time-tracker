<?php

namespace App\Http\Requests;

use App\Factories\TimePeriodsFactory;
use App\ValueObjects\TimePeriods;

class StoreRequest extends Request
{
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'periods.*' => ['required'],
            'periods.*.from' => ['required', 'date'],
            'periods.*.to' => ['required', 'date'],
        ];
    }

    public function name(): string
    {
        return $this->input('name');
    }

    public function periods(): TimePeriods
    {
        return TimePeriodsFactory::create($this->input('periods'));
    }
}
