<?php

namespace App;

use App\ValueObjects\TimePeriods;

class SessionRepository
{
    public function add(string $name, TimePeriods $timePeriods): void
    {
        $session = Session::query()->create([
            'name' => $name,
        ]);

        foreach ($timePeriods->all() as $period) {
            SessionTime::query()->create([
                'session_id' => $session->getKey(),
                'from' => $period->from()->toDateTimeString(),
                'to' => $period->to()->toDateTimeString(),
                'total_time' => $period->durationInSeconds(),
            ]);
        }
    }
}
