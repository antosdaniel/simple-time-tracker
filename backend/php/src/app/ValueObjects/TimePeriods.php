<?php

namespace App\ValueObjects;

class TimePeriods
{
    /** @var TimePeriod[] */
    private $periods = [];

    public function __construct(iterable $periods)
    {
        foreach ($periods as $period) {
            $this->add($period);
        }

        if (empty($this->periods)) {
            throw new \InvalidArgumentException('Empty time periods are not allowed');
        }
    }

    private function add(TimePeriod $period): void
    {
        $this->periods[] = $period;
    }

    /**
     * @return TimePeriod[]
     */
    public function all(): array
    {
        return $this->periods;
    }

    public function durationInSeconds(): int
    {
        $duration = 0;

        foreach ($this->all() as $period) {
            $duration += $period->durationInSeconds();
        }

        return $duration;
    }
}
