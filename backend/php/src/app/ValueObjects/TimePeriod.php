<?php

namespace App\ValueObjects;

use Carbon\Carbon;

class TimePeriod
{
    /** @var Carbon */
    private $from;

    /** @var Carbon */
    private $to;

    /**
     * @param Carbon $from
     * @param Carbon $to
     */
    public function __construct(Carbon $from, Carbon $to)
    {
        $this->from = $from;
        $this->to = $to;

        if ($from->greaterThanOrEqualTo($to)) {
            throw new \InvalidArgumentException(sprintf(
                '"from" has to be after "to", but "%s" is before "%s"',
                $from->toDateTimeString(),
                $to->toDateTimeString()
            ));
        }
    }

    public function from(): Carbon
    {
        return $this->from;
    }

    public function to(): Carbon
    {
        return $this->to;
    }

    public function durationInSeconds(): int
    {
        return $this->to->diffInSeconds($this->from);
    }
}
