<?php

namespace App\Factories;

use App\ValueObjects\TimePeriod;
use App\ValueObjects\TimePeriods;
use Carbon\Carbon;

class TimePeriodsFactory
{
    public static function create(iterable $data): TimePeriods
    {
        $periods = collect($data)
            ->map(function (array $period) {
                $from = Carbon::parse($period['from']);
                $to = Carbon::parse($period['to']);

                if ($from->greaterThanOrEqualTo($to)) {
                    return null;
                }

                return new TimePeriod($from, $to);
            })
            ->filter();

        return new TimePeriods($periods);
    }
}
