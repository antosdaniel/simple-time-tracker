<?php

namespace Tests\Unit\ValueObjects;

use App\ValueObjects\TimePeriod;
use App\ValueObjects\TimePeriods;
use Carbon\Carbon;
use Tests\TestCase;

class TimePeriodsTest extends TestCase
{
    public function test_creation()
    {
        $data = [
            new TimePeriod(new Carbon('now - 5 day'), new Carbon('now - 4 day')),
            new TimePeriod(new Carbon('now - 2 day'), new Carbon('now - 1 day')),
        ];

        $periods = new TimePeriods($data);

        $this->assertEquals($data, $periods->all());
    }

    public function test_empty_creation_fails()
    {
        $this->expectException(\InvalidArgumentException::class);

        new TimePeriods([]);
    }

    public function test_requires_time_period_class()
    {
        $data = [
            new TimePeriod(new Carbon('now - 5 day'), new Carbon('now - 4 day')),
            new \stdClass(),
        ];

        $this->expectException(\TypeError::class);

        new TimePeriods($data);
    }

    public function test_duration_in_seconds()
    {
        $data = [
            new TimePeriod(Carbon::parse('2000-01-01 00:00:00'), Carbon::parse('2000-01-01 00:05:00')),
            new TimePeriod(Carbon::parse('2000-01-01 02:10:00'), Carbon::parse('2000-01-01 04:15:00')),
        ];
        $periods = new TimePeriods($data);

        $result = $periods->durationInSeconds();

        $hoursInSeconds = 2 * 60 * 60;
        $minutesInSeconds = 10 * 60;
        $this->assertEquals($hoursInSeconds + $minutesInSeconds, $result);
    }
}
