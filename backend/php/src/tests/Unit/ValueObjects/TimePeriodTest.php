<?php

namespace Tests\Unit\ValueObjects;

use App\ValueObjects\TimePeriod;
use Carbon\Carbon;
use Tests\TestCase;

class TimePeriodTest extends TestCase
{
    public function test_creation()
    {
        $from = new Carbon('now - 2 days');

        $period = new TimePeriod($from, new Carbon('now - 1 day'));

        $this->assertEquals($from, $period->from());
    }

    public function test_from_has_to_be_before_to()
    {
        $this->expectException(\InvalidArgumentException::class);

        $from = new Carbon('now - 2 days');

        new TimePeriod($from, $from);
    }

    public function test_duration_in_seconds()
    {
        $from = Carbon::parse('2000-01-01 00:00:00');
        $to = Carbon::parse('2000-01-01 00:05:00');
        $period = new TimePeriod($from, $to);

        $result = $period->durationInSeconds();

        $this->assertEquals(5 * 60, $result);
    }
}
