import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {TimePeriod} from './time-period.class';

@Injectable()
export class SessionsRepository {
  private baseUrl: string;

  public constructor(
    private http: HttpClient,
  ) {
    this.baseUrl = `http://${window.location.hostname}:80/api/v1`;
  }

  public all(groupBy: string = 'day'): Observable<any> {
    return this.http.get(`${this.baseUrl}?groupBy=${groupBy}`);
  }

  public add(session: {
    name: string,
    periods: TimePeriod[],
  }): Observable<any> {
    const timeFormat = 'YYYY-MM-DD HH:mm:ss';

    return this.http.post(this.baseUrl, {
      name: session.name,
      periods: session.periods.map((period: TimePeriod) => ({
        from: period.from.format(timeFormat),
        to: period.to.format(timeFormat),
      }))
    });
  }
}
