import {Component} from '@angular/core';
import {Moment} from 'moment';
import {TimePeriod} from '../time-period.class';

@Component({
  selector: 'app-times-list',
  templateUrl: './times-list.component.html',
})
export class TimesListComponent {
  public times: TimePeriod[] = [];
  public format = 'HH:mm:ss';

  public add(time: TimePeriod): void {
    this.times.unshift(time);
  }

  public clear(): void {
    this.times = [];
  }

  public isEmpty(): boolean {
    return this.times.length === 0;
  }

  public totalTime(): Moment {
    let total = 0;

    for (const time of this.times) {
      total += time.durationInMs;
    }

    return TimePeriod.durationToMoment(total);
  }
}
