import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {Moment} from 'moment';
import {finalize} from 'rxjs/operators';
import {SessionsRepository} from '../sessions.repository';
import {TimePeriod} from '../time-period.class';

@Component({
  selector: 'app-sessions-overview',
  templateUrl: './sessions-overview.component.html',
})
export class SessionsOverviewComponent implements OnInit {
  public columns = ['name', 'totalTime'];
  public data: MatTableDataSource<any>;
  public groupBy = 'month';
  public inProgress = false;

  constructor(
    private sessionRepository: SessionsRepository,
  ) { }

  public ngOnInit(): void {
    this.refresh();
  }

  public refresh(): void {
    this.inProgress = true;

    this.sessionRepository.all(this.groupBy)
      .pipe(
        finalize(() => this.inProgress = false),
      )
      .subscribe((data) => {
        this.data = new MatTableDataSource(data.map((raw: any) => {
          return {
            period: raw.period,
            totalTime: this.secondsToMoment(raw.totalTime),
          };
        }));
      });
  }

  private secondsToMoment(timeInSeconds): Moment {
    return TimePeriod.durationToMoment(parseInt(timeInSeconds) * 1000);
  }
}
