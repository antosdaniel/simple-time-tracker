import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule} from '@angular/forms';
import {
  MatButtonModule, MatCardModule, MatIconModule, MatInputModule, MatListModule, MatProgressBarModule, MatSelectModule,
  MatSidenavModule, MatTableModule,
} from '@angular/material';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppComponent} from './app.component';
import {SessionsOverviewComponent} from './sessions-overview/sessions-overview.component';
import {SessionsRepository} from './sessions.repository';
import {TimerComponent} from './timer/timer.component';
import {TimesListComponent} from './times-list/times-list.component';

@NgModule({
  declarations: [
    AppComponent,
    TimerComponent,
    TimesListComponent,
    SessionsOverviewComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,

    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatSidenavModule,
    FlexLayoutModule,
    MatListModule,
    MatInputModule,
    MatProgressBarModule,
    MatTableModule,
    MatSelectModule,
  ],
  providers: [
    SessionsRepository,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
