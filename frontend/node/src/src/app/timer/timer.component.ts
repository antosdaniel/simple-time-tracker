import {Component, EventEmitter, Output} from '@angular/core';
import {Moment} from 'moment';
import {IntervalObservable} from 'rxjs/observable/IntervalObservable';
import {map} from 'rxjs/operators';
import {TimePeriod} from '../time-period.class';
import moment = require('moment');

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
})
export class TimerComponent {
  public startedAt: Moment = null;
  public onTime = IntervalObservable.create().pipe(
    map(() => this.time().toDate()),
  );

  @Output()
  public newTimePeriod = new EventEmitter<TimePeriod>();

  public toggle() {
    if (this.isPlaying()) {
      this.pause();
    } else {
      this.play();
    }
  }

  public isPlaying(): boolean {
    return this.startedAt !== null;
  }

  public play(): void {
    this.startedAt = moment();
  }

  public pause(): void {
    const timePeriod = new TimePeriod(this.startedAt, moment());
    if (timePeriod.duration.seconds() > 0) {
      this.newTimePeriod.next(timePeriod);
    }

    this.startedAt = null;
  }

  private time(): Moment {
    if (!this.isPlaying()) {
      return moment().startOf('day');
    }

    return (new TimePeriod(this.startedAt, moment())).duration;
  }
}
