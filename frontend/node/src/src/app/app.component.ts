import {Component, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {finalize} from 'rxjs/operators';
import {SessionsOverviewComponent} from './sessions-overview/sessions-overview.component';
import {SessionsRepository} from './sessions.repository';
import {TimesListComponent} from './times-list/times-list.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  @ViewChild('timesList')
  public timesList: TimesListComponent;

  @ViewChild('sessionsOverview')
  public sessionsOverview: SessionsOverviewComponent;

  public inProgress = false;

  constructor(
    private sessionRepository: SessionsRepository,
  ) { }

  public save(form: NgForm): void {
    if (form.invalid) {
      return;
    }

    if (this.inProgress) {
      return;
    }

    this.inProgress = true;

    this.sessionRepository.add({
      name: form.value.name,
      periods: this.timesList.times,
    }).pipe(
      finalize(() => this.inProgress = false),
    ).subscribe(() => {
      this.reset(form);
      this.sessionsOverview.refresh();
    });
  }

  public clear(form: NgForm): void {
    this.reset(form);
  }

  private reset(form: NgForm): void {
    form.resetForm();
    this.timesList.clear();
    this.inProgress = false;
  }
}
