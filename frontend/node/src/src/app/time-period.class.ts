import {Moment} from 'moment';
import moment = require('moment');

export class TimePeriod {
  private _from: Moment;
  private _to: Moment;
  private _durationInMs: number;
  private _duration: Moment;

  constructor(from: Moment, to: Moment) {
    this._from = moment(from).startOf('seconds');
    this._to = moment(to).startOf('seconds');
    this._durationInMs = this.to.diff(this.from);
    this._duration = TimePeriod.durationToMoment(this.durationInMs);
  }

  public get from(): Moment {
    return this._from;
  }

  public get to(): Moment {
    return this._to;
  }

  public get duration(): Moment {
    return this._duration;
  }

  public get durationInMs(): number {
    return this._durationInMs;
  }

  public static durationToMoment(duration: number): Moment {
    return moment.utc(duration).subtract(1, 'hour');
  }
}
